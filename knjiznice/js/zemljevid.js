/* global L, distance, $ */

var mapa;
var greenIcon = L.icon({
    iconUrl: 'leaf-green.png',
    shadowUrl: 'leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

window.addEventListener('load', function(){
    var mapOptions = {
    center: [46.05004, 14.46931],
    zoom: 12
    }
    mapa = new L.map('mapa_id', mapOptions);
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    mapa.addLayer(layer);
    
    $.getJSON( "knjiznice/json/bolnisnice.json", function( json ) {
        addDataToMap(json, mapa);
    });
    var popup = L.popup();
    L.marker([46.05004, 14.46931]).addTo(mapa);

    
});
function onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.name) {
        layer.bindPopup(feature.properties.name + " \n <br />Mesto: " + feature.properties['addr:city']);
    }
}
function addDataToMap(data, map) {
    L.geoJson(data, {
        onEachFeature: onEachFeature
    }).addTo(mapa);
}

