/*global $*/
var gen = 0;
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var povVisina
$.getJSON( "knjiznice/json/visina.json", function( json ) {
  povVisina = json[16866]["Mean male height (cm) (centimeters)"];
});

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  var auth = "Basic " + btoa(username + ":" + password);
  
  return auth;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  
  var ehrId = "";

  var ime = ["Janze", "Lojze", "Micka"];
  var priimek = ["Novak", "Šifrer", "Nursdorfer"];

  var diastolic = [90, 100, 110];
  var systolic = [100, 100, 100]
  var height = [185, 180, 140];
  var weight = [110, 75, 50];

  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  
  var call = $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      success: function (data) {

        var ehrId = data.ehrId;
        
        console.log(ehrId);

        // build party data
        var partyData = {
            firstNames: ime[stPacienta],
            lastNames: priimek[stPacienta],
            partyAdditionalInfo: [
                {
                    key: "ehrId",
                    value: ehrId
                }
            ]
        };
        
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
              
              var compositionData = {
                "ctx/language": "en",
                "ctx/territory": "CA",
                "vital_signs/blood_pressure/any_event/diastolic": diastolic[stPacienta],
                "vital_signs/height_length/any_event/body_height_length": height[stPacienta],
                "vital_signs/body_weight/any_event/body_weight": weight[stPacienta]
              };
              
              var queryParams = {
                  "ehrId": data.ehrId,
                  templateId: 'Vital Signs',
                  format: 'FLAT',
                  committer: 'Lenart'
              };
              $.ajax({
                  url: baseUrl + "/composition?" + $.param(queryParams),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(compositionData),
                  success: function (res) {
                    
                    callback(data.ehrId);
                    
                  },
                  error: function(res)
                  {
                    console.log("Something went wrong");
                  }
              });
            },
            error: function(res)
            {
              console.log("Something went wrong");
            }
        });
      },
  });

  
}

function dobiPodatkeForm()
{

  var ehr = $("#ehr").val();
  
  dobiPodatke(ehr);
}

function dobiPodatke(ehrId)
{
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  
  var searchData = [
      {key: "ehrId", value: ehrId}
  ];
  $.ajax({
      url: baseUrl + "/demographics/party/query",
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(searchData),
      success: function (res) {
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/blood_pressure",
            type: 'GET',
            success: function (pressureResult) {
              
                $.ajax({
                    url: baseUrl + "/view/" + ehrId + "/height",
                    type: 'GET',
                    success: function (heightResult) {
                      
                      $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/weight",
                        type: 'GET',
                        success: function (weightResult) {
                          
                          
                          var ime = res['parties'][0]['firstNames'];
                          var priimek = res['parties'][0]['lastNames'];
                          var pritisk = pressureResult[0]['diastolic'];
                          
                          var visina = heightResult[0]['height'];
                          var masa = weightResult[0]['weight'];
                          
                          var table = document.getElementById("tabelaPacientov");

                          
                          document.getElementById("cell1").innerHTML = ime;
                          document.getElementById("cell2").innerHTML = priimek;
                          document.getElementById("cell3").innerHTML = visina;
                          document.getElementById("cell4").innerHTML = masa;
                          document.getElementById("cell5").innerHTML = pritisk;
                          console.log(masa, visina);
                          izracunITM(masa, visina);
                          prikazVisine(visina);
                          prikazMase(masa);
                        }
                     });
                    },
              });
            }
        });
        

      },
      error: function(res) {
        console.log("Error getting data");
      }
  });
}



getAuthorization();

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

  /*global CanvasJS*/
function naslednji()
{
  generirajPodatke(gen, dobiPodatke);
  gen++
}
function prejsnji()
{
  generirajPodatke(gen, dobiPodatke);
  gen--
}


function izracunITM(masa , visina){
  console.log(masa, visina);
  var itm = masa/((visina/100)^2);
  prikazITM(itm)
  console.log(itm);
}
  
function prikazITM(itm){
  
    var chart = new CanvasJS.Chart("ITM", {
  
  animationEnabled: true,
  
  title:{
    text:"Podatki ITM"
  },
  axisX:{
    interval: 1
  },
  axisY2:{
    interlacedColor: "rgba(241, 169, 160, 1)",
    gridColor: "rgba(1,77,101,.1)",
  },
    data: [{
    type: "bar",
    name: "companies",
    axisYType: "secondary",
    color: "#FD0000",
    dataPoints: [
    { y: itm, label: "Podatki pacienta" },
    { y: 22, label: "Idealni ITM" },
    ]
    }]
    });
chart.render();

}
function prikazVisine(visina){

    console.log(povVisina)
    var chart = new CanvasJS.Chart("visina", {
  
  animationEnabled: true,
  
  title:{
    text:"Podatki Visine"
  },
  axisX:{
    interval: 1
  },
  axisY2:{
    interlacedColor: "rgba(1,77,101,.2)",
    gridColor: "rgba(1,77,101,.1)",
  },
    data: [{
    type: "bar",
    name: "companies",
    axisYType: "secondary",
    color: "#014D65",
    dataPoints: [
    { y: visina, label: "Podatki pacienta" },
    { y: povVisina, label: "Slo povprecje" },
    ]
    }]
    });
chart.render();

}

function prikazMase(masa){
  
    var chart = new CanvasJS.Chart("masa", {
  
  animationEnabled: true,
  
  title:{
    text:"Podatki o masi"
  },
  axisX:{
    interval: 1
  },
  axisY2:{
    interlacedColor: "rgba(253, 227, 167, 1)",
    gridColor: "rgba(253, 227, 167, 1)",
  },
    data: [{
    type: "bar",
    name: "companies",
    axisYType: "secondary",
    color: "#EB7327",
    dataPoints: [
    { y: masa, label: "Podatki pacienta" },
    { y: 85, label: "Slo povprecje" },
    ]
    }]
    });
chart.render();

}
function gumbGeneriraj()

{

 for (var i = 3; i > 0; i--)

 {

     generirajPodatke(i, dobiPodatke);
  }
}